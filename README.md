# eafmerge

## Overview

This is a .eaf file merging utility written for the [HLVC
project](http://projects.chass.utoronto.ca/ngn/HLVC/0_0_home.php).
It is designed to work with HLVC-formatted .eaf files so it will probably not be
of much use to you unless you are working on the HLVC project.

## Installation

Run the following commands in the terminal:

1. `git clone https://gitlab.com/gadanidis/eafmerge` (download the source code)
2. `cd eafmerge` (enter the source code directory)
3. `pip3 install --user .` (install the script)

Make sure the script location is in PATH by adding the following line to
`.bash_profile`:

```
export PATH=$PATH:"/Users/hlvc/Library/Python/3.8/bin
```

## Usage

### Single IDs

In the terminal, type:

```
eafmerge -s SEARCH_DIR -o OUTPUT_DIR -t TYPE solo INTERVIEW_ID
```

- `SEARCH_DIR` is the directory you want to search for eaf files (e.g.,
  `"RUSSIAN (Heritage)"`)
- `OUTPUT_DIR` is the directory where the output file should be saved (e.g.,
  `merged`)
- `solo` indicates that you want to process a single file
- `INTERVIEW_ID` is the interview ID code (e.g., `I1M61A_IV`).

Note that the interview ID must include the type of interview (e.g., `IV` or
`FW`), and, if the interview has multiple parts, must include the number (e.g.,
if the `I1M61A_IV` interview has two parts, you must run `I1M61A_IV_1` and
`I1M61A_IV_2` separately, rather than just `I1M61A_IV`).

You can leave out `-s SEARCH_DIR` and `-o OUTPUT_DIR` and the script will search
the current directory and save the result to the current directory.
You can also leave out `-t TYPE` and the script will search for `IV` files by
default.
The options can be entered in any order, except that `solo INTERVIEW_ID` must
always be at the end.

If the directory you want to search has special characters (e.g., spaces or
brackets), make sure to enclose the name in quotes (so type `"RUSSIAN
(Heritage)"` rather than just `RUSSIAN (Heritage)`.

After running the script, try opening the created file in ELAN to make sure that
the file was created successfully and it is openable.

### Multiple IDs at once

If you want to merge a lot of IDs at once, you can put the IDs in a file, one
per line, like below:

```
I1M61A
I1F61A
I2M40A
...
```

Then, in the terminal, type:

```
eafmerge -s SEARCH_DIR -o OUTPUT_DIR -t TYPE batch ID_FILE
```

- `SEARCH_DIR` is the directory you want to search for eaf files (e.g.,
  `"RUSSIAN (Heritage)"`)
- `OUTPUT_DIR` is the directory where the output file should be saved (e.g.,
  `merged`)
- `TYPE` is the type of file to search for (e.g., `IV` or `FW`)
- `batch` indicates that you want to process multiple IDs
- `ID_FILE` is a text file containing multiple interview IDs, one per line

The options can be entered in any order, except that `batch ID_FILE` must always
be at the end.

### Step-by-step guide for HLVC RAs

This is a step-by-step guide to running the script.
For more details on usage, check out the subsections above.

1. Create a new folder inside the folder for the language you want to merge
(e.g., create a folder named `eaf_merging` folder inside the `RUSSIAN
(HERITAGE)` folder).
2. Inside `eaf_merging`, create an empty folder named `output`.
3. Also inside `eaf_merging`, create a text file named `ids.txt` that contains
the list of speaker IDs that should be merged, one ID per line:
```
I1M61A
I1F61A
I2M40A
...
```
4. Open the terminal on the iMac, either using the Terminal app on the iMac
itself, or remotely using `ssh`.
    - to use `ssh` on macOS or Linux, open a terminal and type ssh
      [USERNAME]@[IP\_ADDRESS], then enter the password.
    - to use `ssh` on Windows, use [PuTTY](https://www.putty.org/).
5. Navigate to the `eaf_merging` folder using the `cd` command, using slashes to
separate folder names:
```
cd "RUSSIAN (HERITAGE)/eaf_merging"
```
6. Enter the following command:
```
eafmerge -s .. -o output batch ids.txt > log.txt
```
7. Wait for the program to finish.
Once it is done, the merged eaf files will be in the `output` directory, and
there will be a log file (`log.txt`) which you can look at to see if there were
any issues during the merge.

Contact Tim at `timothy.gadanidis@mail.utoronto.ca` if you have any issues.

## Logging

The script is fragile and may fail if it encounters a problem I did not
anticipate. When running the program in batch mode, I recommend redirecting the
output to an external file so that it can be inspected later.

You can do that using the shell redirector `>`:

```
eafmerge batch files.txt > log.txt
```

The above command will save all the program's output to the file `log.txt`,
which you can later inspect to see if the program reported any warnings or
inserted any tiers that might be problematic.

## Validating output

ELAN has an EAF validation feature that can be used to identify why a file may
not be opening. In ELAN, under the `File` menu option, click `Validate EAF
File...` to access this feature.

#!/usr/bin/env python3

from distutils.core import setup

setup(name='eafmerge',
      version='1.0',
      decription='HLVC eaf file merging utility',
      author='Tim Gadanidis',
      author_email='tim@gadanidis.ca',
      scripts=['eafmerge'],
      install_requires=['bs4', 'python-Levenshtein', 'lxml']
      )
